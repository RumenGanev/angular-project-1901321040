import {Component, Input, OnInit} from '@angular/core';
import {Advert} from "../../models/advert";
import {UserService} from "../../services/userService";
import {AdvertService} from "../../services/advertService";

@Component({
  selector: 'app-advert-details',
  templateUrl: './advert-details.component.html',
  styleUrls: ['./advert-details.component.css']
})
export class AdvertDetailsComponent implements OnInit {
  @Input() advert!: Advert
  userService = UserService.instance
  advertService = AdvertService.instance

  constructor() { }

  ngOnInit(): void {
  }

}

import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserService} from "../../services/userService";
import {AdvertService} from "../../services/advertService";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userService = UserService.instance
  @Output() editAccountEvent = new EventEmitter<boolean>()

  constructor() { }

  ngOnInit(): void {
  }

  changeFilter(filter: string) {
    AdvertService.instance.filter = filter
  }

  editAccount() {
    this.editAccountEvent.emit(true)
  }
}

import {Component, OnInit} from '@angular/core';
import {AdvertService} from "../../services/advertService";
import {UserService} from "../../services/userService";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})

export class MainComponent implements OnInit {
  userService = UserService.instance
  userDetailsShowing: boolean = false
  registerShowing: boolean = false

  constructor() {
  }

  ngOnInit(): void {
  }

  editAccount() {
    this.userDetailsShowing = true
  }

  editComplete() {
    this.userDetailsShowing = false
  }

  registerAccount() {
    this.registerShowing = true
  }

  registerComplete() {
    this.registerShowing = false
  }
}

import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserService} from "../../services/userService";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @Output() registerEventEmitter = new EventEmitter<boolean>()
  userService = UserService.instance
  email: string = ''
  name: string = ''
  password: string = ''

  emailError: boolean = false
  nameError: boolean = false
  passwordError: boolean = false

  constructor() {
  }

  ngOnInit(): void {
  }

  register(email: string, name: string, password: string) {
    if(email === "" || password === "") {
      this.passwordError = password === ""
      this.nameError = name === ""
      this.emailError = email === ""
      return
    }

    this.userService.registerUser(email, name, password)
    this.registerComplete()
  }

  registerComplete() {
    this.registerEventEmitter.emit(true)
  }
}

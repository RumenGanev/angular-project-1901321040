import {Component, Input, OnInit} from '@angular/core';
import {AdvertService} from "../../services/advertService";
import {Advert} from "../../models/advert";
import {UserService} from "../../services/userService";

@Component({
  selector: 'app-adverts',
  templateUrl: './adverts.component.html',
  styleUrls: ['./adverts.component.css']
})
export class AdvertsComponent implements OnInit {
  advertService: AdvertService
  userService: UserService
  selectedAdvert?: Advert

  constructor() {
    this.advertService = AdvertService.instance
    this.userService = UserService.instance
  }

  ngOnInit(): void {
  }

  viewAdvert(advert: Advert) {
    this.selectedAdvert = advert
  }

  applyForAdvert() {
    this.advertService.addApplicant(this.selectedAdvert!.id, UserService.instance.currentUser!.id)
  }

  back() {
    this.selectedAdvert = undefined
  }
}

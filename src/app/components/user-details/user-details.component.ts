import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserService} from "../../services/userService";

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  @Output() editCompleteEmitter = new EventEmitter<boolean>()
  userService = UserService.instance
  email: string;
  name: string;
  password: string;

  emailError: boolean = false
  nameError: boolean = false
  passwordError: boolean = false

  constructor() {
    this.email = this.userService.currentUser!.email;
    this.name = this.userService.currentUser!.name;
    this.password = ""
  }

  ngOnInit(): void {
  }

  editUser(email: string, name: string, password: string) {
    if(email === "" || password === "") {
      this.passwordError = password === ""
      this.nameError = name === ""
      this.emailError = email === ""
      return
    }

    this.userService.currentUser!.editAccount(email, name, password)
    this.editComplete()
  }

  editComplete() {
    this.editCompleteEmitter.emit(false)
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertItemsComponent } from './advert-items.component';

describe('AdvertItemsComponent', () => {
  let component: AdvertItemsComponent;
  let fixture: ComponentFixture<AdvertItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvertItemsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdvertItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

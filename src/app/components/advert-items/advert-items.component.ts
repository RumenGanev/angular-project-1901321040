import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Advert} from "../../models/advert";

@Component({
  selector: 'app-advert-items',
  templateUrl: './advert-items.component.html',
  styleUrls: ['./advert-items.component.css']
})
export class AdvertItemsComponent implements OnInit {
  @Input() advert!: Advert
  @Output() viewAdvert = new EventEmitter<Advert>()

  constructor() { }

  ngOnInit(): void {
  }

  viewAdvertDetails() {
    this.viewAdvert.emit(this.advert)
  }
}

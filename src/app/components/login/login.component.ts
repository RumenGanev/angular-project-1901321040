import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserService} from "../../services/userService";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Output() registerEmitter = new EventEmitter<boolean>()
  email: string = ""
  password: string = ""
  isCompany = 0

  emailError: boolean = false
  passwordError: boolean = false
  wrongDataError = false

  constructor() { }

  ngOnInit(): void {
  }

  logIn(email: string, password: string) {
    console.log(UserService.instance.users)
    this.passwordError = password === ''
    this.emailError = email === ''

    if(email === '' || password === '') {
      return
    }

    for(let user of UserService.instance.users.values()) {
      if(user.email === email && user.password === password) {
        UserService.instance.currentUser = user
        console.log(user)
        return
      }
    }
    this.wrongDataError = true
  }

  register() {
    this.registerEmitter.emit(true)
  }
}

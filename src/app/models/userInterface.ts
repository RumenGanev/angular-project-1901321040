
export interface UserInterface {
  id: string;
  email: string;
  name: string;
  password: string;

  getUser(): UserInterface
  editAccount(email: string, name: string, password: string): void
  deleteAccount(): void
}

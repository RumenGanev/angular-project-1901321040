import {v4 as uuid} from 'uuid';
import {testUsers} from "../users/user";
import {Company, testCompanies} from "../users/company";

export class Advert {
  readonly id: string;
  title: string;
  description: string;
  type: AdvertType;
  category: AdvertCategory;
  applicants: Map<string, boolean>;
  ratings: Map<string, number>;
  salary: number;
  publisher: string

  constructor(
    title: string,
    description: string,
    type: AdvertType,
    category: AdvertCategory,
    applicants: Map<string, boolean>,
    ratings: Map<string, number>,
    salary: number,
    publisher: string
  ) {
    this.id = uuid();
    this.title = title;
    this.description = description;
    this.type = type;
    this.category = category;
    this.applicants = applicants;
    this.ratings = ratings;
    this.salary = salary
    this.publisher = publisher
  }

  getId(): string {
    return this.id
  }

  getRating(): number {
    let total = 0
    let count = 0
    for(let rating of this.ratings.values()) {
      total += rating
      count++
    }
    return Math.floor(total / count)
  }
}

export enum AdvertType {
  fullTime = "Full Time Job Offer",
  partTime = "Part Time Job Offer",
  remote = "Remote Job Offer"
}

export enum AdvertCategory {
  officeAdmin = "Office Administration",
  development = "Development",
  qa = "QA"
}

export function testAdverts(): Advert[] {
  const users = testUsers
  const companies = testCompanies

  let temp: Advert[] = []

  for (let i = 0; i < 6; i++) {
    let userMap = new Map<string, boolean>()
    let ratingMap = new Map<string, number>()

    for(let user of users.values()) {
      if((Math.round(Math.random()) % 2 === 0)) {
        userMap.set(user.id, false)
        ratingMap.set(user.id, Math.floor(Math.random() * 10 % (10 ?? 0) + (10 ? 0 : 0)))
      }
    }

    let advert = new Advert(
      `Advert ${i}`,
      `Description ${i}`,
      AdvertType.fullTime,
      AdvertCategory.development,
      userMap,
      ratingMap,
      Math.round(Math.random() * 1800 % (1800 ?? 400) + (1800 ? 400 : 400)),
      companies[Math.floor(Math.random() * 2 % (2 ?? 0) + (2 ? 0 : 0))].id
    )

    temp.push(advert)
  }

  return temp
}

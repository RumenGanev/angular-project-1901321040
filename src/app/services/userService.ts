import {testUsers, User} from "../users/user";
import {UserInterface} from "../models/userInterface";

export class UserService {
  static readonly instance = new UserService()

  users: UserInterface[]
  currentUser?: UserInterface = undefined

  isLoggedIn(): boolean {
    return this.currentUser !== undefined
  }

  private constructor() {
    this.users = testUsers
  }

  registerUser(email: string, name: string, password: string): void {
    this.users.push(new User(email, name, password))
  }

  getUser(id: string): UserInterface {
    for(let user of this.users) {
      if(user.id === id) {
        return user
      }
    }
    return this.users[0]
  }

  getUsers(ids: string[]): UserInterface[] {
    let temp: UserInterface[] = []

    for(let id of ids.values()) {
      temp.push(this.getUser(id))
    }

    return temp
  }

  deleteUser(userId: string): void {
    let index = this.users.findIndex(user => {
      return user.id === userId
    })
    if(index != null) {
      this.users.splice(index, 1)
    }
  }

  logOut() {
    this.currentUser = undefined
    console.log(this.users)
  }
}

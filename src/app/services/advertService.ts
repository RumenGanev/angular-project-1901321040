import {Advert, AdvertType, AdvertCategory, testAdverts} from '../models/advert';
import {User} from "../users/user";
import {UserService} from "./userService";
import {UserInterface} from "../models/userInterface";



export class AdvertService {
  static readonly instance = new AdvertService();

  adverts: Advert[];
  filter?: string

  private constructor() {
    this.adverts = testAdverts();
  }

  newAdvert(title: string,
            description: string,
            likes: number,
            type: AdvertType,
            category: AdvertCategory,
            applicants: Map<string, boolean>,
            ratings: Map<string, number>,
            salary: number,
            publisher: string): void {
    this.adverts.push(new Advert(title, description, type, category, applicants, ratings, salary, publisher));
  }

  deleteAdvert(id: string): void {
    const index = this.adverts.findIndex(element => {
      return element.id === id;
    })

    if(index == undefined) {
      console.log('Error: no such element found')
      return
    }
    this.adverts.splice(index, 1)
  }

  getAdvert(id: string): Advert | undefined {
    return this.adverts.find(element => {
      return element.id === id;
    })
  }

  addApplicant(advertId: string, userId: string): void {
    this.adverts.forEach(advert => {
      if(advert.id === advertId) {
        if(!advert.applicants.has(userId)) {
          advert.applicants.set(userId, false);
        }
      }
    })
  }

  removeApplicant(advertId: string, userId: string): void {
    this.adverts.forEach(advert => {
      if(advert.id === advertId) {
        if(advert.applicants.has(userId)) {
          advert.applicants.delete(userId);
        }
      }
    })
  }

  approveApplicant(advertId: string, userId: string): void {
    this.adverts.forEach(advert => {
      if(advert.id === advertId) {
        for(let value of advert.applicants.values()) {
          if(value) {
            return
          } else {
            advert.applicants.set(userId, true);
          }
        }
      }
    })
  }

  rateAdvert(advertId: string, userId: string, rating: number): void {
    if(rating < 0) { rating = 0 }
    else if (rating > 5) { rating = 5 }
    this.adverts.forEach(advert => {
      if(advert.id === advertId) {
        advert.ratings.set(userId, rating)
      }
    })
  }

  getAdvertRatings(advertId: string): number {
    this.adverts.forEach(advert => {
      if(advert.id === advertId) {
        let total: number = 0
        let index: number = 0
        for(let value of advert.ratings.values()) {
          index++;
          total+=value;
        }
        return total/index;
      }
      return 0
    })
    return 0
  }

  getPersonalApplications(userId: string): Advert[] {
    let temp: Advert[] = []
    this.adverts.forEach(advert => {
      if(advert.applicants.has(userId)) {
        temp.push(advert)
      }
    })

    return temp
  }

  getApplicantList(advertId: string): UserInterface[] {
    let idTemp: string[] = []
    this.adverts.forEach(advert => {
      for(let applicantId of advert.applicants.keys()) {
        idTemp.push(applicantId)
      }
    })

    return UserService.instance.getUsers(idTemp)
  }

  isApproved(userId: string, advertId: string): boolean {
    let advert = this.getAdvert(advertId)
    if(advert!.applicants.has(userId)) {
      return false
    }

    return advert!.applicants.get(userId)!
  }
}

import {v4 as uuid} from 'uuid';
import {UserInterface} from "../models/userInterface";
import {UserService} from "../services/userService";

export class User implements UserInterface {
  id: string;
  email: string;
  name: string;
  password: string;

  constructor(email: string, name: string, password: string) {
    this.id = uuid();
    this.email = email;
    this.name = name;
    this.password = password;
  }

  getId(): string {
    return this.id;
  }

  changePassword(password: string): void {
    this.password = password
  }

  changeName(name: string): void {
    this.name = name
  }

  deleteAccount(): void {
    UserService.instance.deleteUser(this.id)
  }

  editAccount(email: string = this.email, name: string = this.name, password: string = this.password): void {
    this.email = email
    this.name = name;
    this.password = password;
  }

  getUser(): UserInterface {
    return this;
  }
}

export var testUsers = [
  new User("pav@gmail.com", "Pavel", "pav123"),
  new User("restanta@abv.bg", "Samuil", "asd123"),
  new User("griskom@gmail.com", "Lina", "password"),
  new User("12315123@gmail.com", "Ivan", "qwertyuiop"),
  new User("kikoite@yahoo.com", "Kristian", "neshtositam"),
  new User("rumen", "rumen", "rumen"),
]

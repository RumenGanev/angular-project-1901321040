import {UserInterface} from "../models/userInterface";
import {v4 as uuid} from 'uuid';
import {UserService} from "../services/userService";

export class Company implements UserInterface{

  id: string;
  email: string;
  name: string;
  password: string;

  constructor(email: string, name: string, password: string) {
    this.id = uuid()
    this.email = email;
    this.name = name;
    this.password = password;
  }

  getUser(): UserInterface {
    return this;
  }

  editAccount(email: string = this.email, name: string = this.name, password: string = this.password): void {
    this.email = email
    this.name = name;
    this.password = password;
  }

  deleteAccount() {
    UserService.instance.deleteUser(this.id)
  }
}

export var testCompanies = [
  new Company("asrr@co.com", "ASRR Ltd.", "password"),
  new Company("arcDev@co.com", "ArcDevelopment", "password"),
  new Company("test", "test", "test")
]

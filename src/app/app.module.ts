import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {RouterOutlet} from "@angular/router";
import {MainComponent} from "./components/main/main.component";
import { LoginComponent } from './components/login/login.component';
import {FormsModule} from "@angular/forms";
import { AdvertsComponent } from './components/advertsList/adverts.component';
import { HeaderComponent } from './components/header/header.component';
import { AdvertDetailsComponent } from './components/advert-details/advert-details.component';
import { AdvertItemsComponent } from './components/advert-items/advert-items.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { RegisterComponent } from './components/register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    AdvertsComponent,
    HeaderComponent,
    AdvertDetailsComponent,
    AdvertItemsComponent,
    UserDetailsComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    RouterOutlet,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
